# ScoopTalk

![alt text](images/site/logo/logo_base_flat_300-150.png?raw=true)

ScoopTalk is a Content Management System similar to WordPress.


## Features


## Documentation
Will be up after deemed needed and where we stand on the development. Some might be found here on [the wiki](../../wikis/home).

The upcoming [main developers site](https://2l8-projects.org/) may have some documentation and if needed a forum.


## License
BSD 3-clause License. See [LICENSE](LICENSE) for more details.

## Contibute
See the [CONTRIBUTING](CONTRIBUTING.md) file for detailed information.


## Development, Bugs, Feature Requests, etc.
If you want development info, place a bug report etc. can be done in [the ISSUES area](../../issues).

To see our development plans, just visit the [Milestones](../../milestones).
